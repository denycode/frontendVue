import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import SBN from '../views/SBN.vue'
import Reksadana from '../views/Reksadana.vue'
import ConventionalOSF from '../views/osf/Conventional.vue'
import ProductiveOSF from '../views/osf/Productive.vue'
import ProductiveInvoice from '../views/invoice/Productive.vue'
import ConventionalInvoice from '../views/invoice/Conventional.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/sbn',
    name: 'SBN',
    component: SBN
  },
  {
    path: '/reksadana',
    name: 'reksadana',
    component: Reksadana
  },
  {
    path: '/osf/conventional',
    name: 'conventional',
    component: ConventionalOSF
  },
  {
    path: '/osf/productive',
    name: 'productive',
    component: ProductiveOSF
  },
  {
    path: '/invoice/productive',
    name: 'productive',
    component: ProductiveInvoice
  },
  {
    path: '/invoice/conventional',
    name: 'conventional',
    component: ConventionalInvoice
  }
]

const router = new VueRouter({
  routes
})

export default router
